
if (Drupal.jsEnabled) {
  /**
   * Lots of help from:
   * http://remysharp.com/2007/01/25/jquery-tutorial-text-box-hints/
   */
  jQuery.fn.hide_and_seek = function() { return this.each(function() {
    var label = jQuery(this).prev('label').hide();
    jQuery(this).next('.description').hide();

    // get jQuery version of 'this'
    // NOTE: since jQuery() returns a cached value that gets destroyed
    // this can't be done until after the hide's above.
    var t = jQuery(this);

    // set the default value based on the label
    var labelvalue = label[0].innerHTML;
    if (labelvalue) {
      var pos = labelvalue.indexOf(':');
      if (pos >= 0) {
        var required = labelvalue.indexOf(': <') >= 0 ? ' (*)' : '';
        labelvalue = labelvalue.substr(0, pos) + required;
      }
      t.defaultvalue = labelvalue;
    }
    // @TODO: if there isn't a label, take the default from the description

    if (t.defaultvalue) {
      // on blur, set value to title attr if text is blank
      t.blur(function(){
        if (t.val() == '') {
          t.val(t.defaultvalue);
          t.addClass('blur');
        }
      });

      // on focus, set value to blank if current value matches defaultvalue
      t.focus(function(){
        if (t.val() == t.defaultvalue) {
          t.val('');
          t.removeClass('blur');
        }
      });

      // now change all inputs to title
      t.blur();
    }
  });} 

  $(document).ready(
    function() {
      $('.block-signit input').hide_and_seek();
      $('.block-signit form').submit(function() {
        $('.block-signit input').focus();
      });
    }
  );
}
