
The SignIt module is a means of conducting online signature gathering and
sending of those signatures. It provides options for sending or just displaying
the signatures on the site. It has a fair degree of CiviCRM integration, but
does not depend on CiviCRM. In the future, much more robust support for CiviCRM
will be added.

SignIt is basically a fork of the Petition module (http://drupal.org/project/petition)
It does, however, offer different funcitonality and has taken a significantly different
direction. That being said, thanks to Morbus for a good foundation to start from.

Each signup form can be converted into a custom block by first selecting the
"Use Block" option on the node edit form, and then by adding the block from the
admin/build/block page.

This module was developed for DefectiveByDesign (http://defectivebydesign.org) 
and sponsored by CivicActions (http://civicactions.com).


